Title : Evidence for the violation of lepton universality in beauty meson decays

Journal : Nature Physics

Contact authors : Daniel Moise, Davide Lancierini, Samuel Maddrell-Mander

Reviewers : Tim Gershon (chair), Frederic Teubert, Wouter Hulsbergen

EB reviewer : Francesco Dettori

EB readers : Stephane Monteil, Jolanta Brodzicka

Analysis note : ANA-2019-016

Deadline : 26-Feb-2021

e-group : lhcb-paper-2021-004-reviewers

CDS link : https://cds.cern.ch/record/2751965

Authors : LHCb (list of 9-Feb-2021)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/BuToKllLFUFullRun2

---

Please send your comments to:
https://codimd.web.cern.ch/g7FVMvU2Sjmu5oZRpDO5Cg?both

A copy of the paper draft and note can be found at:
https://disk.pku.edu.cn:443/link/D3146C8A9875999BF095F8D17C061FB4

Responsible: Ao Xu
