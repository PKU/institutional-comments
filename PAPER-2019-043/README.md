Title : Search for the lepton flavour violating decay $B^+ to K^+ mu^- tau^+$ using $B^{*0}_{s2}$ decays

Journal :

Contact authors : Matthew Rudolph

Reviewers : Giampiero Mancinelli (chair), Carla Marin

EB reviewer : John Walsh

EB readers : Stephane Monteil, Sergey Barsuk

Analysis note : ANA-2018-040

Deadline : 17-Jan-2020

e-group : lhcb-paper-2019-043-reviewers

CDS link : https://cds.cern.ch/record/2705461

Authors : LHCb (list of 10-Dec-2019)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Bs2stKmutau
