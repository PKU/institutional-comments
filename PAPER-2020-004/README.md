Title : Observation of new $ Xi_c^0$ baryons decaying to $ Lambda_c^+K^-$

Journal : PRL

Contact authors : Emmy Gabriel, Jibo He, Marco Pappagallo, Zhihao Xu

Reviewers : Marco Adinolfi (chair), Shanzhen Chen, Mark Williams

EB reviewer : Sergey Barsuk

EB readers : Stephane Monteil, John Walsh

Analysis note : LHCb-ANA-2019-010

Deadline : 27-Feb-2020

e-group : lhcb-paper-2020-004-reviewers

CDS link : https://cds.cern.ch/record/2709437

Authors : LHCb (list of 11-Feb-2020)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Xicst2LcK
