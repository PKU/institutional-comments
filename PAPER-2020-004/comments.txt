General comments:

In the systematic uncertainty studies, different configurations of angular momentum and Weisskopf factors are tested. It may be appropriate to mention the default values in the nominal fit.


Abstract:

- Suggest to add a comma after 13\tev in the first sentence.

- Suggest to be more quantitative about the "large significance" as have been done in the text, e.g. "over 20\sigma".

- It may be more appropriate to say "impefect knowledge" instead of "knowledge".


Line-by-line comments:

L2 Maybe "light" is sufficient to refer to (di)quarks composed of u,d quarks.

L15 Suggest to add "if otherwise stated", since in L149 you refer to particles without charge conjugation.

L17 Please check here and some other places that the space between "B^-" and "\to" is correct.

L46 Suggest "good quality inconsistent" -> "good quality that are inconsistent" such that it is compitible with "and have large" in L47.

L56 It is not clear what is the chi2 of "the flight distance".

L62 It may be more clear to add the discriminating variable of the sPLot technique.

L62 Suggest to use "BDT" instead of the "multivariate algorithm" to be more specific.

L64 It is not clear from the text why the use of 1% data can minimise the bias.

L73 Please be more specific about "inconsistent with the known phi(1020)" as it has a natural width besides the mass resolution.

L81 Why the mass windo here (+-20) is different from the mass window (+-15) used to optimise the BDT?

L87 "is the large" -> "is due to the large".

L88 Suggest to move L90 here to explain the meaning of symbols in the FoM.

L93 "of pT" -> "of pT of the".

L96 "distribution of" -> "distributions of".

L98 "is described" -> "is obtained".

L98 What quantity is the "fraction" averaged over?

L101 Suggest to remove "and are therefore..." since it is not the only reason to draw this conclusion.

L103 Remove "the result of".

L104 The bracket is not correct in the decay here and several other lines.

L109 "relativistic Breit-Wigner" -> "relativistic Breit-Wigner function".

L115 It is not clear how these contributions are shifted.

L116 Can you explain more about the "isospin argument"?

L131 "peak." -> "peak, where".

L138 It is not clear how "a structure" is modelled.

L139 "was" -> "is".

L140 "data taking" -> "data-taking".

L149 "conjugate" -> "charge conjugate".

L165 "i" is both subscript and imaginary unit in the equation.

L173 "is applied to" -> "is considered in".


Tables and Figures:

- Table 1:
Caption: "natural width and ..." -> "natural width \Gamma and ..."
The first column is not 'invariant mass difference \Delta M', suggest change to 'mean of peak' or 'peak position'.

- Table 2:
Caption: systematic errors -> systematic uncertainties
Data-MC discrepancy -> Data-simulation discrepancy

- Table 3:
Caption: first error, second error -> first uncertainty, second uncertainty
Peak of \Delta M -> 'Mean of peak' or 'Peak position'

- Figure 1:
The data points should be on top of the fit curve.
The fit is not mentioned in the text.
Suggest to add a legend on the figure.
The pion in the x title is not in italic.

- Figure 2:
It might be better to put Fig. 2(b) and Fig. 3 together for comparison.
(a) You should use filled style for legend `Lc sideband K-` and `Lc+ K+` as shown in the plot.
(b) There's a blue filled component which is not specified in legend.
You should use filled style for legend `Xic -> Lc K pi` component as shown in the plot.
The signal components are plotted in dashed line, which is inconsistent with legend.
The legend might be too small.

- Figure 3:
Solve the inconsistency between plotted components and legend, as suggested for Fig. 2(b).
The color and line style of `Structure` are too close to the `Xic(2965) -> Lc K` component,
which may cause confusion. Please use another color.




