General comments:

- The general comments1.

- The general comments2.


Line-by-line comments:

Abstract:
add a comma after 13\tev:
the expression 'large significance' is inaccurate, suggest using: ... are
observed with significance over 20\sigma, and their masses ...


L78 why only 20% of the candidates are shown here?
L82 ... is the mass resolution -> is the resolution from mass fit.
L90 why Xic(2930) -> Lc K is not observed in the selected data in
260<DeltaM<290 region, while its simulated sample is treated as signal?
L94 \pt the Xic candidate -> \pt of the Xic candidates
    as well as a tight requirement on the kaon PID -> as well as a tight PID
    requirement on the kaon originated from Xic. (should specify which kaon it
    is)
L109 relativistic Breit-Wigner -> relativistic Breit-Wigner function


Tables and Figures:

- Table 1:
Caption: natural width and ... -> natural width \Gamma and ...
         the first column is not 'invariant mass difference \Delta M', suggest
         change to 'mean of peak' or 'peak position'

- Table 2:
Caption: systematic errors -> systematic uncertainties
Data-MC discrepancy -> Data-simulation discrepancy

- Table 3:
Caption: first error, second error -> first uncertainty, second uncertainty
Peak of \Delta M -> 'Mean of peak' or 'Peak position'

- Figure 1:
suggest to add a legend on the figure

- Figure 2:
what's the point of putting these two plots together? I think it's better to
put Fig. 2(b) and Fig. 3 together for comparison.
(a) you should use filled style for `Lc sideband K-` and `Lc+ K+` as shown in the
plot
(b) there's a blue filled component which is not specified in legend
    you should use filled style for `Xic -> Lc K pi` component as shown in the
    plot
    the signal components are plotted in dashed line, which is inconsistent
    with legend

- Figure 3:
solve the inconsistency between plotted components and legend, as suggested
for Fig. 2(b).
the color of `Structure` is too close to the `Xic(2965) -> Lc K` component,
which may cause confusion. Please use another color.
