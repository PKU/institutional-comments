Please commit your comments to the relevant folder following the steps below:
1. Clone the project.
```
git clone ssh://git@gitlab.cern.ch:7999/PKU/institutional-comments.git
```
2. Copy and rename `comments_template.txt` to the target folder.
```
copy comments_template.txt PAPER-20XX-XXX/comments_your_name.txt
```
3. Follow the links in `PAPER-20XX-XXX/README.md` and make your comments.
```
vi PAPER-20XX-XXX/comments_your_name.txt
```
4. Submit your comments.
```
git add PAPER-20XX-XXX/comments_your_name.txt
git commit -m "comments from your_name"
git push -u origin master
```


