Title : Search for low-mass dimuon resonances

Journal : JHEP

Contact authors : Xabier Cid Vidal, Phil Ilten, Constantin Weisser, Mike Williams

Reviewers : Nicola Serra (chair), Karlis Dreimanis, Stephen Farry

EB reviewer : John Walsh

EB readers : Marcello Rotondo, Konstantinos Petridis

Analysis note : ANA-2019-038

Deadline : 26-May-2020

e-group : lhcb-paper-2020-013-reviewers

CDS link : https://cds.cern.ch/record/2717877

Authors : LHCb (list of 11-May-2020)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/DarkPhotonRun2
