Title : Precision measurement of the $\Xi_b^-$ baryon lifetime

Journal : Physical Review D

Contact authors: Steven R. Blusk

WG readers: Elisabetta Spadaro Norella, Lorenzo Capriotti, Miroslauv Saur  

Referees: Jiayin Sun (chair), Ricardo Vazquez Gomez

EB reviewer: Yuehong Xie 

EB readers : Maximilien Chefdeville; Lars Eklund

Analysis note : ANA-2024-001

Deadline : 8-04-2024

e-group : lhcb-paper-2024-010-reviewers

CDS link : https://cds.cern.ch/record/2892688

Authors : LHCb (https://lbfence.cern.ch/membership/authorship/manage-list-for-paper?id=2645 )

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/XibMinus2XicZeroHDecays

---

Please send your comments to:

https://codimd.web.cern.ch/77TWq1PfTlKcSB85ADAPew?both#


Responsible: Yuhao Wang
