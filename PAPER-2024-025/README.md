Title : Observation of muonic Dalitz decays of $\chi_b$ mensons and precise spectroscopy of hidden beauty

Journal : JHEP

Contact authors: Matthew David Needham, Vanya Belyaev

WG readers: Elisabetta Spadaro Norella, Lorenzo Capriotti, Miroslauv Saur  

Referees: Patrick Koppenburg (chair), John Walsh (member)

EB reviewer: Matthew William Kenzie

EB readers : Yuehong Xie, Agnieszka Dziurda

Analysis note : LHCb-ANA-2024-029

Deadline : 2024-07-04

e-group : lhcb-paper-2024-025-reviewers

CDS link : https://cds.cern.ch/record/2901617

Authors : 

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Chibmumu

---

Please send your comments to:

https://codimd.web.cern.ch/iSVw3qS9SL2vvzSzD_RXNg


Responsible: Yuyang Jiang
