Title : Observation of $omega$ contribution in the $X(3872) to pi^+ pi^- J/ psi$ decays

Journal : Physical Review Letter

Proponents: Baasansuren Batsukh (Baaska), Tomasz Skwarnicki, Mikhail Mikhasenko, Alessandro Pilloni, Cesar Fernandez-Ramirez, Adam Szczepaniak

WG readers: Dmitrii Pereima, Giovanni Cavallero, Pavel Krokovny

Referees: Liming Zhang (chair), Andrii Usachov

EB readers : Philip James Ilten

Analysis note : ANA-2021-022

Deadline : 22-12-2021

e-group : lhcb-paper-2021-045-reviewers

CDS link : https://cds.cern.ch/record/2792538?

Authors : LHCb (list of 2021-11-09)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/X3872ToJpsiPiPi

---

Please send your comments to:

https://codimd.web.cern.ch/ZtF3EGsvQQqy6jNOpr9SBg?both


Responsible: Zhihong Shen
