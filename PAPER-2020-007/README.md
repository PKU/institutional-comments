Title : Searches for 25 rare and forbidden decays of $D^+$ and $D_s^+$ mesons

Journal : JHEP

Contact authors : Christopher Burr, Chris Parkes

Reviewers : Maurizio Martinelli (chair), Claire Prouve, Siim Tolk

EB reviewer : Stephane Monteil

EB readers : Sergey Barsuk, Carla Gobel

Analysis note : ANA-2018-027

Deadline : 30-Apr-2020

e-group : lhcb-paper-2020-007-reviewers

CDS link : https://cds.cern.ch/record/2715372

Authors : LHCb (list of 10-Mar-2020)

Twiki : https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/D2hll2016
