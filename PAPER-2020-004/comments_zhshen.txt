** General comments:

- The general comments1.

- The general comments2.


** Line-by-line comments:

L37 between -> among
L51 originate -> originating
L93 of $p_T$ -> of $p_T$ of
L194 have -> has


** Tables and Figures:

- Tablexx: 

- Figure 2(b) and Figure 3: the first line and the sixth line of the legend use nearly the same color.
                            the sixth line of the legend should not use line. (I guess)
									 the first three line of the legend should not be solid line.


** References:

- Refxx:
